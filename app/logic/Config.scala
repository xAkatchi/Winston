package logic

import pdi.jwt.JwtAlgorithm

/**
  * This class contains the global config for this application (outside of the application.conf)
  *
  * Eventually the constants from here might be moved to the application.conf, but they're here
  * right now for convenience.
  *
  * TODO move everything to the application.conf file
  */
object Config {
  val JwtKey: String = "cN2erjFSGf$etE4or^JeoV5q1i1e8S$$!#KE9b0!kxxgRk345hsT*R5eeX6L"
  val JwtAlgorithmType: JwtAlgorithm.HS256.type = JwtAlgorithm.HS256
  val JwtTokenValidityInSeconds: Int = 86400 * 31 // 31 days (86400 seconds is 1 day)

  val WolframAlphaBaseUrl = "http://api.wolframalpha.com/v1/result"
  val WolframAlphaAppId = "9G29JV-6GYRTAYWTW"
  val WolframAlphaUnits = "metric"
}
