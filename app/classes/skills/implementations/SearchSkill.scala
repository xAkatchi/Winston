package classes.skills.implementations

import javax.inject.Inject

import classes.skills.Skill
import logic.Config
import play.api.libs.ws.WSClient
import play.api.libs.ws.ahc.AhcCurlRequestLogger

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.Duration

/**
  * The goal of this Skill is to provide search capabilities to Winston.
  *
  * @param ws
  * @param ec
  */
class SearchSkill @Inject()
  (ws: WSClient)
  (implicit ec: ExecutionContext)
  extends Skill
{
  /**
    * This method will send the given query to the Wolfram Alpha Short Answers API,
    * the result of this API will then be returned
    *
    * [[https://products.wolframalpha.com/short-answers-api/documentation/]]
    *
    * @param query
    *
    * @return
    */
  def queryWolfram(query: String): Future[String] = {
    val response = ws
      .url(Config.WolframAlphaBaseUrl)
      .withRequestFilter(AhcCurlRequestLogger())
      .addQueryStringParameters(("appid", Config.WolframAlphaAppId), ("i", query), ("units", Config.WolframAlphaUnits))
      .withRequestTimeout(Duration("10 seconds"))
      .get()

    response.map(_.body)
  }

  override def getType: String = "search"

  /**
    * Currently we can always handle the given query, this because we query an external
    * API, thus have no influence over the logic ourselves.
    *
    * TODO - Check whether or not we have queries left for htis month here
    *
    * @param query
    *
    * @return
    */
  override def canHandleQuery(query: String): Boolean = true

  /**
    * Whenever this method is invoked, we'll send the given query through to the
    * Wolfram Alpha API.
    *
    * @param query
    *
    * @return
    */
  override def resolve(query: String): Future[String] =
    queryWolfram(query)
}
