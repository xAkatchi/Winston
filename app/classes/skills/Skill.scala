package classes.skills

import scala.concurrent.Future

/**
  * A skill is basically a 'thing' that Winston can do.
  *
  * A 'simple' example of this would be a skill that provides search
  * capabilities, which can then be used to ask Winston things like:
  * 'When was the golden age'.
  */
trait Skill extends APIRepresentationSkill
{
  /**
    * Implementations of this method should return whether or not they can handle
    * the given query.
    *
    * @param query
    *
    * @return
    */
  def canHandleQuery(query: String): Boolean

  /**
    * This method should only be invoked after the `canHandleQuery` returned true.
    * Implementations of this method will not check whether or not they could handle
    * the query, they will assume that they can handle the query if this method is invoked.
    *
    * Implementations of this method should do the 'real work' of this skill inside
    * this method.
    *
    * @param query
    *
    * @return
    */
  def resolve(query: String): Future[String]
}
