package classes.skills

/**
  * The goal of this trait is to provide methods which can be used when building
  * an API representation of a skill.
  */
trait APIRepresentationSkill
{
  /**
    * This method returns the type of the skill, this is mainly used by the
    * clients of the API so that they can add custom logic based on which
    * skill was being used by Winston.
    *
    * @return
    */
  def getType: String
}
