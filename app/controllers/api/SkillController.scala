package controllers.api

import javax.inject.{Inject, Singleton}

import classes.skills.Skill
import persistence.dao.user.UserDAO
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.mvc._
import utils.actions.{JwtAuthenticatedAction, RequiredPermissionsAction}
import utils.bodyparsers.ValidateJson

import scala.concurrent.{ExecutionContext, Future}

/**
  * The goal of the SkillController related endpoints is to execute a query
  * from the authenticated user.
  *
  * This can vary depending on the injected skills, the 'simplest' example
  * would be a 'search' query (e.g. Who is the president of the USA)
  *
  * @param cc                 Standard controller components
  * @param jwtAuth            Used to secure the endpoints
  * @param userDao
  * @param validateJson       The body parser to validate a request with a given Reads[T]
  * @param skills             The skills which will be used when trying to resolve the query from the user
  * @param ec                 Required by Play to execute our asynchronous code
  */
@Singleton
class SkillController @Inject
  (cc: ControllerComponents, userDao: UserDAO,
   jwtAuth: JwtAuthenticatedAction, validateJson: ValidateJson, skills: Set[Skill])
  (implicit ec: ExecutionContext)
  extends AbstractController(cc)
{
  case class Query(query: String)

  /**
    * This val is being used to parse / validate the given input JSON data to
    * the Query case class.
    *
    * Since it's a single field, we can't unwrap the reads to a tuple, thus we
    * have to do it like this (also see the SO link for more info).
    *
    * [[https://stackoverflow.com/a/40787852 reference]]
    */
  implicit val queryReads: Reads[Query] = (__ \ "query")
    .read[String](maxLength[String](2000))
    .map(Query)

  // TODO somehow put the body in the end instead of front so that permnissions are checked first
  // TODO somehow figure out how we can use DI for the permission action (gives issues with the required permission string that cant be set)
  def resolveQuery: Action[Query] = (Action(validateJson[Query]) andThen jwtAuth andThen RequiredPermissionsAction(userDao, "USE_SKILLS")) async { request =>
    val query = request.body.query

    skills.find(_.canHandleQuery(query)) match {
      case Some(skill) =>
        skill.resolve(query).map(result =>
          Ok(Json.obj(
            "type" -> skill.getType,
            "result" -> result,
          ))
        )
      case None => Future.successful(NotFound(Json.obj(
        "type" -> "not_found",
        "result" -> Json.obj(
          "message" -> "No matching skill found for the given query"
        )
      )))
    }
  }
}