package controllers.api

import javax.inject.{Inject, Singleton}

import logic.Config
import org.apache.commons.lang3.time.DateUtils
import org.mindrot.jbcrypt.BCrypt
import pdi.jwt.JwtJson
import persistence.dao.user.UserDAO
import persistence.entities.User
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import play.api.mvc._
import utils.bodyparsers.ValidateJson

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.Duration

/**
  * The goal of this controller is to handle all the utils.actions that are related to the user accounts
  * (e.g. registration / login).
  *
  * @param userDao The DAO which will be used when handling persistence related utils.actions
  *                (e.g. new account creations)
  * @param cc      Standard controller components
  * @param ec      Required by Play to execute our asynchronous code
  */
@Singleton
class UserAccountController @Inject()
  (userDao: UserDAO, cc: ControllerComponents, validateJson: ValidateJson)
  (implicit ec: ExecutionContext)
  extends AbstractController(cc)
{
  case class LoginData(email: String, password: String)
  case class RegistrationData(firstName: String, lastName: String, email: String, password: String)

  /**
    * The goal of this validator is to check whether or not the given email
    * is unique.
    */
  private val uniqueEmailValidation = Reads.StringReads.filter(
    JsonValidationError("Email not unique"))(
    email => Await.result(userDao.findByEmail(email).map(_.isEmpty), Duration("5 seconds"))
  )

  /**
    * This val is being used to parse / validate the given input JSON data to the LoginData case class.
    */
  implicit val loginDataReads: Reads[LoginData] = (
    (__ \ "email").read[String](email) and
    (__ \ "password").read[String]
  )(LoginData.apply _)

  /**
    * This val is being used to parse / validate the given input JSON data to the RegistrationData case class.
    */
  implicit val registrationDataReads: Reads[RegistrationData] = (
    (__ \ "firstName").read[String] and
    (__ \ "lastName").read[String] and
    (__ \ "email").read[String](email andKeep uniqueEmailValidation) and
    (__ \ "password").read[String]
  )(RegistrationData.apply _)

  /**
    * This method handles the authentication of a user.
    *
    * If the given credentials are valid, it will return a JWT token which can be used
    * on subsequent requests, if the credentials are invalid, a Unauthorized will be returned.
    *
    * @return Either a Unauthorized in the case of invalid credentials, or an Ok in the case of valid
    *         credentials
    */
  def login: Action[LoginData] = Action(validateJson[LoginData]).async { implicit request =>
    val data = request.body

    userDao
      .findByEmail(data.email)
      .flatMap(maybeUser =>
        maybeUser
          .find(user => user.isActive && BCrypt.checkpw(data.password, user.password))
          .map { user =>
            // The exp / iat times should be in seconds, thus we divide it by 1000 to get the second representation
            val exp = (System.currentTimeMillis() / 1000) + Config.JwtTokenValidityInSeconds
            val iat = System.currentTimeMillis() / 1000

            val claim = Json.obj(
              ("user_id", user.id),
              ("exp", exp),
              ("iat", iat),
            )

            Future.successful(Ok(Json.obj(
              "token" -> JwtJson.encode(claim, Config.JwtKey, Config.JwtAlgorithmType),
              "expires_at" -> exp
            )))
          }.getOrElse(
            Future.successful(Unauthorized)
          )
      )
  }

  /**
    * This method handles the registration of new user accounts.
    *
    * A new account will only be created whenever the given data is valid (see the registrationDataReads
    * val for more details about the validation).
    *
    * @return Either a BadRequest if the validation failed, or an Ok message when the user is successfully created
    */
  def register: Action[RegistrationData] = Action(validateJson[RegistrationData]).async { implicit request =>
    val data = request.body

    userDao.insert(User(
      None,
      data.firstName,
      data.lastName,
      data.email,
      BCrypt.hashpw(data.password, BCrypt.gensalt()),
      isActive = false
    ))

    Future.successful(Ok(Json.obj("status" -> "OK", "message" -> ("User '" + data.email + "' created.") )))
  }
}
