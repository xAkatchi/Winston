package persistence.entities

/**
  * This entity maps to the 'user_permission' table inside the database.
  *
  * @param userId
  * @param permissionId
  */
case class UserPermission (
  userId: Long,
  permissionId: Long
)
