package persistence.entities

/**
  * This entity maps to the 'user' table inside the database.
  *
  * @param id
  * @param firstName
  * @param lastName
  * @param email
  * @param password
  * @param isActive
  */
case class User(
  id: Option[Long] = None,
  firstName: String,
  lastName: String,
  email: String,
  password: String,
  isActive: Boolean
);
