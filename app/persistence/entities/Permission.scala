package persistence.entities

/**
  * This entity maps to the 'permission' table inside the database.
  *
  * @param id
  * @param name
  */
case class Permission (
  id: Option[Long] = None,
  name: String
);
