package persistence.dao.permission

import persistence.entities.Permission

import scala.concurrent.Future

/**
  * The goal of implementations of this class is to handle permission related persistence
  * logic (e.g. being able to find permissions by their name)
  */
trait PermissionDAO
{
  /**
    * Implementations of this method should return an Option[Permission] containing either the Permission
    * with the given name, or None if no permission with the given name exists.
    *
    * @param name The name which should be used when looking for a permission
    *
    * @return Either the found Some(Permission) or None if no permission could be found
    */
  def findByName(name: String): Future[Option[Permission]]

  /**
    * Implementations of this method should store the given Permission.
    *
    * @param permission The permission which should be stored
    *
    * @return
    */
  def insert(permission: Permission): Future[Unit]
}
