package persistence.dao.permission

import javax.inject.{Inject, Singleton}

import persistence.entities.Permission
import persistence.slick.permission.PermissionsComponent
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
  * An implementation of the PermissionDAO using a Database (through Slick)
  * as storage layer.
  *
  * @param dbConfigProvider The slick database that this DAO is using internally, bound through Module.
  * @param executionContext A CPU bound execution context. Slick manages blocking JDBC calls with its
  *                         own internal thread pool, so Play's default execution context is fine here.
  */
@Singleton
class DatabasePermissionDAO @Inject()
(protected val dbConfigProvider: DatabaseConfigProvider)
(implicit executionContext: ExecutionContext)
  extends HasDatabaseConfigProvider[JdbcProfile]
    with PermissionDAO
    with PermissionsComponent
{
  import profile.api._

  /**
    * This method tries to find a permission with the given name
    *
    * @param name The name which should be used when looking for a permission
    *
    * @return Either the found Some(Permission) or None if no permission could be found
    */
  override def findByName(name: String): Future[Option[Permission]] =
    db.run(permissions.filter(_.name === name).result.headOption)

  /**
    * This methid will insert the given permission into the database.
    *
    * @param permission The permission which should be stored
    *
    * @return
    */
  override def insert(permission: Permission): Future[Unit] =
    db.run(permissions += permission).map(_ => ())
}
