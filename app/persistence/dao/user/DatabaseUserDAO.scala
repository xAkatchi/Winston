package persistence.dao.user

import javax.inject.{Inject, Singleton}

import persistence.entities.{Permission, User}
import persistence.slick.user_permission.UserPermissionsComponent
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

/**
  * An implementation of the UserDAO using a Database (through Slick)
  * as storage layer.
  *
  * @param dbConfigProvider The slick database that this DAO is using internally, bound through Module.
  * @param executionContext A CPU bound execution context. Slick manages blocking JDBC calls with its
  *                         own internal thread pool, so Play's default execution context is fine here.
  */
@Singleton
class DatabaseUserDAO @Inject()
  (protected val dbConfigProvider: DatabaseConfigProvider)
  (implicit executionContext: ExecutionContext)
  extends HasDatabaseConfigProvider[JdbcProfile]
    with UserDAO
    with UserPermissionsComponent
{
  import profile.api._

  /**
    * This method tries to find a User with the given id
    *
    * @param id The id which should be used when looking for a user
    *
    * @return Either the found Some(User) or None if no user could be found
    */
  override def findById(id: Long): Future[Option[User]] =
    db.run(users.filter(_.id === id).result.headOption)

  /**
    * This method tries to find a User with the given email
    *
    * @param email The email which should be used when looking for a user
    *
    * @return Either the found Some(User) or None if no user could be found
    */
  override def findByEmail(email: String): Future[Option[User]] =
    db.run(users.filter(_.email === email).result.headOption)

  /**
    * This method returns the permissions that are linked to the given user
    *
    * @param user The user who's permissions should be returned
    *
    * @return A list of permissions that belong to the given user
    */
  override def getPermissionsFromUser(user: User): Future[Seq[Permission]] = {
    val query = for {
      (user_permission, permission) <- user_permissions join permissions on (_.permissionId === _.id)
      if user_permission.userId === user.id
    } yield permission

    db.run(query.result)
  }

  /**
    * This method will insert the given user into the database.
    *
    * @param user The user which should be stored
    * @return
    */
  override def insert(user: User): Future[Unit] =
    db.run(users += user).map(_ => ())
}
