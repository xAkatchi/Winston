package persistence.dao.user

import persistence.entities.{Permission, User}

import scala.concurrent.Future

/**
  * The goal of implementations of this class is to handle user related persistence
  * logic (e.g. being able to find users by their email)
  */
trait UserDAO
{
  /**
    * Implementations of this method should return an Option[User] containing either the User
    * with the given id, or None if no user with the given id exists.
    *
    * @param id The id which should be used when looking for a user
    *
    * @return Either the found Some(User) or None if no user could be found
    */
  def findById(id: Long): Future[Option[User]]

  /**
    * Implementations of this method should return an Option[User] containing either the User
    * with the given email, or None if no user with the given email exists.
    *
    * @param email The email which should be used when looking for a user
    *
    * @return Either the found Some(User) or None if no user could be found
    */
  def findByEmail(email: String): Future[Option[User]]

  /**
    * Implementations of this method should return all the Permission objects that are
    * linked to the given user.
    *
    * @param user The user who's permissions should be returned
    *
    * @return A list of permissions that belong to the given user
    */
  def getPermissionsFromUser(user: User): Future[Seq[Permission]]

  /**
    * Implementations of this method should store the given User.
    *
    * @param user The user which should be stored
    *
    * @return
    */
  def insert(user: User): Future[Unit]
}
