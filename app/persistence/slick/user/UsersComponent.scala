package persistence.slick.user

import persistence.entities.User
import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the Users table.
  *
  * Extend this trait whenever you have a desire to do something with the user table.
  * The `users` value is already defined and thus can be used straight away to query the table.
  *
  * [[https://github.com/playframework/play-slick/blob/master/samples/computer-database/app/dao/ComputersDAO.scala
  * Used as inspiration]]
  */
trait UsersComponent { self: HasDatabaseConfigProvider[JdbcProfile] =>
  import profile.api._

  protected val users = TableQuery[Users]

  /**
    * This class contains a mapping from the database table to an object.
    * ([[http://slick.lightbend.com/doc/3.0.0/gettingstarted.html#schema as required by Slick]])
    *
    * @param tag
    */
  protected class Users(tag: Tag) extends Table[User](tag, "user")
  {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def firstName = column[String]("first_name")
    def lastName = column[String]("last_name")
    def email = column[String]("email")
    def password = column[String]("password")
    def isActive = column[Boolean]("is_active")

    def * = (id.?, firstName, lastName, email, password, isActive) <> (User.tupled, User.unapply)
  }
}