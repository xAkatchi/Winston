package persistence.slick.permission

import persistence.entities.Permission
import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the Permissions table.
  *
  * Extend this trait whenever you have a desire to do something with the permission table.
  * The `permissions` value is already defined and thus can be used straight away to query the table.
  */
trait PermissionsComponent { self: HasDatabaseConfigProvider[JdbcProfile] =>
  import profile.api._

  val permissions = TableQuery[Permissions]

  /**
    * This class contains a mapping from the database table to an object.
    *
    * @param tag
    */
  class Permissions(tag: Tag) extends Table[Permission](tag, "permission")
  {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")

    def * = (id.?, name) <> (Permission.tupled, Permission.unapply)
  }
}
