package persistence.slick.user_permission

import persistence.entities.UserPermission
import persistence.slick.permission.PermissionsComponent
import persistence.slick.user.UsersComponent
import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile

/**
  * A trait containing a reference to the UserPermissions table.
  *
  * Extend this trait whenever you have a desire to do something with the user_permission table.
  * The `user_permissions` value is already defined and thus can be used straight away to query the table.
  */
trait UserPermissionsComponent
  extends UsersComponent
  with PermissionsComponent
{
  self: HasDatabaseConfigProvider[JdbcProfile] =>
  import profile.api._

  val user_permissions = TableQuery[UserPermissions]

  /**
    * This class contains a mapping from the database table to an object.
    *
    * @param tag
    */
  class UserPermissions(tag: Tag) extends Table[UserPermission](tag, "user_permission")
  {
    def userId = column[Long]("user_id")
    def permissionId = column[Long]("permission_id")

    def * = (userId, permissionId) <> (UserPermission.tupled, UserPermission.unapply)

    def userIdFK =
      foreignKey("user_id_fk", userId, users)(_.id, onDelete = ForeignKeyAction.Cascade)
    def permissionIdFK =
      foreignKey("permission_id_fk", permissionId, permissions)(_.id, onDelete = ForeignKeyAction.Cascade)
  }
}
