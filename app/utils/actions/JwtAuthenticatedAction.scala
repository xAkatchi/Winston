package utils.actions

import javax.inject.Inject

import logic.Config
import pdi.jwt.JwtJson
import persistence.dao.user.UserDAO
import persistence.entities.User
import play.api.libs.json.Json
import play.api.mvc._
import utils.requests.AuthenticatedUserRequest

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}

/**
  * The goal of this Action is to validate whether or not the incoming request
  * has a valid JWT Authorization header set.
  *
  * If this is not the case, the request will be terminated otherwise the next
  * action will be invoked.
  *
  * @param bodyParser
  * @param userDao Used to fetch/validate the user when creating a new AuthenticatedUserRequest
  *                if the given JWT token is valid.
  * @param ec
  */
case class JwtAuthenticatedAction @Inject()
  (bodyParser: PlayBodyParsers, userDao: UserDAO)
  (implicit ec: ExecutionContext)
  extends ActionBuilder[AuthenticatedUserRequest, AnyContent]
  with ActionRefiner[Request, AuthenticatedUserRequest]
{
  /**
    * This Action checks whether or not a valid JWT token has been given inside the
    * 'Authorization' header. If this is the case, the action will be transformed to contain
    * the authenticated user and then it will be let through, otherwise an Unauthorized page will be returned.
    *
    * The token will be expected in the following format:
    * 'Authorization: Bearer <token>'
    *
    * @return Either an Unauthorized page, or the next action depending on whether or not a valid
    *         token is present (including the authenticated user).
    */
  override protected def refine[A](request: Request[A]): Future[Either[Result, AuthenticatedUserRequest[A]]] = {
    val maybeRequest = for {
      authHeader <- getAuthorizationHeaderFromRequest(request)
      token <- getTokenFromAuthorizationHeader(authHeader)
      // TODO not sure how to not have the eitherUser here
      user <- Await.result(getActiveUserFromToken(token), Duration("5 seconds"))
    } yield new AuthenticatedUserRequest[A](user, request)

    Future.successful(maybeRequest)
  }

  /**
    * This method will return either the Authorization header from the given request
    * or an Result in the case of an error.
    *
    * @param request
    * @tparam B
    *
    * @return
    */
  def getAuthorizationHeaderFromRequest[B](request: Request[B]): Either[Result, String] = {
    request.headers.get("Authorization") match {
      case Some(authHeader) => Right(authHeader)
      case None => Left(Results.Unauthorized.withHeaders(
        "WWW-Authenticate" -> "Bearer realm=\"Secured\", error=\"missing_token\"",
      ))
    }
  }

  /**
    * This method will return either the token from the Authorization header or
    * an Result in the case of an error.
    *
    * @param authHeader
    *
    * @return
    */
  def getTokenFromAuthorizationHeader(authHeader: String): Either[Result, String] = {
    val authHeaderPieces = authHeader.split(" ")
    val maybeBearer = authHeaderPieces.headOption
    val maybeToken = authHeaderPieces.drop(1).headOption

    maybeBearer match {
      case Some(authentication_type) =>
        if (authentication_type == "Bearer") {
          maybeToken
            .map(Right(_))
            .getOrElse(Left(Results.Unauthorized.withHeaders(
              "WWW-Authenticate" -> "Bearer realm=\"Secured\", error=\"missing_token\"",
            )))
        } else {
          Left(Results.Unauthorized.withHeaders(
            "WWW-Authenticate" -> "Bearer realm=\"Secured\", error=\"invalid_authentication_type\"",
          ))
        }
      case None => Left(Results.Unauthorized.withHeaders(
        "WWW-Authenticate" -> "Bearer realm=\"Secured\", error=\"invalid_header\"",
      ))
    }
  }

  /**
    * This method will either return the user as set inside the given token or
    * an Result in the case of an error.
    *
    * @param token
    *
    * @return
    */
  def getActiveUserFromToken(token: String): Future[Either[Result, User]] = {
    JwtJson.decode(token, Config.JwtKey, Seq(Config.JwtAlgorithmType)) match {
      case Success(result) =>
        val userId: Long = (Json.parse(result.toJson) \ "user_id").asOpt[Long].getOrElse(-1)
        userDao
          .findById(userId)
          .map(maybeUser =>
            maybeUser
              .find(user => user.isActive)
              .map(Right(_))
              .getOrElse(Left(Results.Unauthorized.withHeaders(
                "WWW-Authenticate" -> "Bearer realm=\"Secured\", error=\"invalid_user\"",
              )))
          )
      case Failure(e) => Future.successful(Left(Results.Unauthorized.withHeaders(
        "WWW-Authenticate" -> "Bearer realm=\"Secured\", error=\"invalid_token\"",
      )))
    }
  }

  override def parser: BodyParser[AnyContent] = bodyParser.anyContent
  override protected def executionContext: ExecutionContext = ec
}
