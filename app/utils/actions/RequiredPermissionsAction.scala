package utils.actions

import javax.inject.Inject

import persistence.dao.user.UserDAO
import play.api.mvc._
import utils.requests.AuthenticatedUserRequest

import scala.concurrent.{ExecutionContext, Future}

/**
  * The goal of this action is to check whether or not the user inside the
  * [[AuthenticatedUserRequest]] has the given permission.
  *
  * @param userDao            Used to obtain all the permissions from the user
  * @param requiredPermission The permission to check for
  * @param ec                 Required by Play to execute our asynchronous code
  */
case class RequiredPermissionsAction
  (userDao: UserDAO, requiredPermission: String)
  (implicit ec: ExecutionContext)
  extends ActionFilter[AuthenticatedUserRequest]
{
  /**
    * This method specifically checks whether or not the user inside the given
    * request has the specified permission.
    *
    * @param request
    * @tparam A
    * @return
    */
  override protected def filter[A](request: AuthenticatedUserRequest[A]): Future[Option[Result]] = {
    val userPermissions = userDao.getPermissionsFromUser(request.user)

    userPermissions.map(permissions =>
      permissions
        .find(_.name == requiredPermission)
        .map { _ =>
          None
        }.getOrElse {
          Some(Results.Forbidden)
        }
    )
  }

  override protected def executionContext: ExecutionContext = ec
}
