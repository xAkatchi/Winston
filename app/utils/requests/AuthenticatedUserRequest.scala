package utils.requests

import persistence.entities.User
import play.api.mvc.{Request, WrappedRequest}

/**
  * An extension on the Request which can be used to add the currently authenticated user
  * to the request.
  *
  * [[https://www.playframework.com/documentation/2.6.x/ScalaActionsComposition#adding-information-to-requests documentation]]
  *
  * @param user    The currently authenticated user
  * @param request The current request
  * @tparam A
  */
class AuthenticatedUserRequest[A](val user: User, request: Request[A]) extends WrappedRequest[A](request)

