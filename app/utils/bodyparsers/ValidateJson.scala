package utils.bodyparsers

import javax.inject.Inject

import play.api.libs.json.{JsError, Reads}
import play.api.mvc._

import scala.concurrent.ExecutionContext

/**
  * The goal of this BodyParser is to parse and validate the JSON (from the user input)
  * it will return a BadRequest if the parsed JSON fails the validation.
  *
  * [[https://www.playframework.com/documentation/2.6.x/ScalaJsonHttp#creating-a-new-entity-instance-in-json Source]]
  *
  * @return Either a BadRequest if the validation fails or the BodyParser if the validation
  *         was successful.
  */
class ValidateJson @Inject()(bodyParser: PlayBodyParsers) extends BodyParsers {
  def apply[T](implicit reads: Reads[T], ec: ExecutionContext): BodyParser[T] = {
    bodyParser.json.validate(userInput =>
      userInput
        .validate[T]
        .asEither
        .left
        .map(e => Results.BadRequest(JsError.toJson(e)))
    )
  }
}
