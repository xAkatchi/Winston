import classes.skills.Skill
import classes.skills.implementations.SearchSkill
import com.google.inject.AbstractModule
import net.codingwell.scalaguice.{ScalaModule, ScalaMultibinder}
import persistence.dao.user.{DatabaseUserDAO, UserDAO}

/**
  * This class is a Guice module that tells Guice how to bind several
  * different types. This Guice module is created when the Play
  * application starts.
  *
  * [[https://www.playframework.com/documentation/2.6.x/ScalaDependencyInjection#programmatic-bindings Documentation]]
  */
class Module extends AbstractModule with ScalaModule
{
  override def configure(): Unit = {
    bind(classOf[UserDAO]).to(classOf[DatabaseUserDAO])

    // Bind all the available skills below (using: https://github.com/codingwell/scala-guice)
    val skills = ScalaMultibinder.newSetBinder[Skill](binder)
    skills.addBinding.to(classOf[SearchSkill])
  }
}
