#FROM openjdk:8
#
#RUN apt-get update -yqq
#RUN apt-get install apt-transport-https -yqq
#  # Add keyserver for SBT
#RUN echo "deb http://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list
#RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 642AC823
#  # Install SBT
#RUN apt-get update -yqq
#RUN apt-get install sbt -yqq
#
#  # Copied below from: https://hub.docker.com/r/ysihaoy/scala-play/
#  # Not sure how much of this is needed, but this works and makes it so that the first request
#  # doesn't have to be compiled
#COPY ["build.sbt", "/tmp/build/"]
#COPY ["project/plugins.sbt", "project/build.properties", "/tmp/build/project/"]
#RUN cd /tmp/build && \
# sbt compile && \
# sbt test:compile && \
# rm -rf /tmp/build
#
#RUN mkdir /usr/src/app
#COPY . /usr/src/app
#WORKDIR /usr/src/app
#
#RUN sbt compile && sbt test:compile
#EXPOSE 9000
#
#CMD ["sbt"]

FROM hseeberger/scala-sbt

RUN mkdir /usr/src/app
COPY . /usr/src/app
WORKDIR /usr/src/app

EXPOSE 9000

CMD ["sbt"]

#FROM ysihaoy/scala-play:2.12.2-2.6.0-sbt-0.13.15
#
## caching dependencies
#COPY ["build.sbt", "/tmp/build/"]
#COPY ["project/plugins.sbt", "project/build.properties", "/tmp/build/project/"]
#RUN cd /tmp/build && \
# sbt compile && \
# sbt test:compile && \
# rm -rf /tmp/build
#
## copy code
#WORKDIR /root/app
#ADD . /root/app/
#RUN sbt compile && sbt test:compile
#
#EXPOSE 9000
#CMD ["sbt"]