# Adds the basic fundamentals for the permissions
# This migration also creates the junction table that links the permission
# to users.

# --- !Ups

CREATE TABLE permission (
  id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255) NOT NULL UNIQUE COMMENT
    'This value will likely be used throughout the application to check whether or not a user has a given permission.
    So if such a value should be changed, do so with caution!'
);

CREATE TABLE user_permission (
  user_id INT(11) NOT NULL,
  permission_id INT(11) NOT NULL,

  CONSTRAINT user_permission_pk PRIMARY KEY (user_id, permission_id),
  CONSTRAINT user_id_fk FOREIGN KEY (user_id) REFERENCES user (id),
  CONSTRAINT permission_id_fk FOREIGN KEY (permission_id) REFERENCES permission (id)
);

# --- !Downs

DROP TABLE permission;
DROP TABLE user_permission;