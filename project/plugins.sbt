logLevel := Level.Warn

resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"

addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.6.10")
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.1")