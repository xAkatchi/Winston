name := "Winston"
version := "1.0"
scalaVersion := "2.12.2"
lazy val `winston` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"

libraryDependencies ++= Seq(
  ehcache,
  ws,
  guice,
  "com.typesafe.play" %% "play-slick" % "3.0.0",
  "com.typesafe.play" %% "play-slick-evolutions" % "3.0.0",
  "mysql" % "mysql-connector-java" % "5.1.41",
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % "test",
  "org.mindrot" % "jbcrypt" % "0.4",
  "com.pauldijou" %% "jwt-play-json" % "0.14.1",
  "net.codingwell" %% "scala-guice" % "4.1.1",
  specs2 % Test
)
unmanagedResourceDirectories in Test <+= baseDirectory ( _ /"target/web/public/test" )